package leo.delegate;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import leo.dao.WordDao;
import leo.dao.ejb3.WordDaoEjb3;
import leo.transfers.WordTo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Leo
 */
public class WordQuery {

    private static final Logger logger = LoggerFactory.getLogger(WordQuery.class);
    private WordDao wordDao = null;

    public String query(String word) {
        logger.debug("查询的单词是：{}", word);
        String result = null;

        if (null != word && (!word.equals(""))) {
            WordTo wordTo = wordDao.getWordByEnglish(word);

            if (null != wordTo) {
                result = wordTo.getChinese();
            }
        }
        if (null == result || result.equals("")) {
            result = "记录为空";
        }

        logger.debug("返回的结果是：{}", result);
        return result;
    }
    private static WordQuery wordQuery = null;

    private WordQuery() {
        try {
            InitialContext ctx = new InitialContext();
            wordDao = (WordDao) ctx.lookup(WordDaoEjb3.class.getName());
        } catch (NamingException ex) {

            logger.error(ex.toString());

        }
    }

    public static WordQuery getInstance() {
        if (wordQuery == null) {
            wordQuery = new WordQuery();
        }
        return wordQuery;
    }
}
