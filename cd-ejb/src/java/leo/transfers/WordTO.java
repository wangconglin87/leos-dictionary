package leo.transfers;

import java.io.Serializable;

/**
 *
 * @author Leo
 */
public class WordTo implements Serializable{
    private Long wordId;
    private String english;
    private String chinese;

    public WordTo(String english, String chinese) {
        this.english = english;
        this.chinese = chinese;
    }

    public WordTo(Long wordId, String english, String chinese) {
        this.wordId = wordId;
        this.english = english;
        this.chinese = chinese;
    }

    public String getChinese() {
        return chinese;
    }

    public void setChinese(String chinese) {
        this.chinese = chinese;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public Long getWordId() {
        return wordId;
    }

    public void setWordId(Long wordId) {
        this.wordId = wordId;
    }
}
