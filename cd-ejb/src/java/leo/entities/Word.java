package leo.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Leo
 */
@Entity
@Table(name = "yh")
@NamedQueries({
    @NamedQuery(name = "Word.findAll", query = "SELECT w FROM Word w"),
    @NamedQuery(name = "Word.findByWordId", query = "SELECT w FROM Word w WHERE w.wordId = :wordId"),
    @NamedQuery(name = "Word.findByEnglish", query = "SELECT w FROM Word w WHERE w.english = :english")
})
public class Word implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long wordId;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "english")
    private String english;
    @Lob
    @Size(max = 65535)
    @Column(name = "chinese")
    private String chinese;

    public Word() {
    }

    public Word(Long wordId) {
        this.wordId = wordId;
    }

    public Word(Long wordId, String english) {
        this.wordId = wordId;
        this.english = english;
    }

    public Word(Long wordId, String english, String chinese) {
        this.wordId = wordId;
        this.english = english;
        this.chinese = chinese;
    }

    public Long getWordId() {
        return wordId;
    }

    public void setWordId(Long wordId) {
        this.wordId = wordId;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getChinese() {
        return chinese;
    }

    public void setChinese(String chinese) {
        this.chinese = chinese;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wordId != null ? wordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Word)) {
            return false;
        }
        Word other = (Word) object;
        if ((this.wordId == null && other.wordId != null) || (this.wordId != null && !this.wordId.equals(other.wordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "leo.dao.Word[ wordId=" + wordId + " ]";
    }
}
