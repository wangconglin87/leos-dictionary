package leo.dao;

import java.util.ArrayList;
import leo.transfers.WordTo;

/**
 *
 * @author Leo
 */
public interface WordDao {
    public WordTo getWordByEnglish(String english);
    public ArrayList<WordTo> getWordsByEnglish(String english);
    
}
