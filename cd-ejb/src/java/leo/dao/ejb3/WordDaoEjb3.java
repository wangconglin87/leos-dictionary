package leo.dao.ejb3;

import javax.ejb.Remote;
import leo.dao.WordDao;

/**
 *
 * @author Leo
 */
@Remote
public interface WordDaoEjb3 extends WordDao {
    
}
